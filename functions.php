<?php
/**
 * Flatflowers-theme Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package flatflowers-theme
 */

add_action( 'wp_enqueue_scripts', 'storefront_parent_theme_enqueue_styles' );

/**
 * Enqueue scripts and styles.
 */
function storefront_parent_theme_enqueue_styles() {
	wp_enqueue_style( 'storefront-style', get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( 'flatflowers-theme-style',
		get_stylesheet_directory_uri() . '/style.css',
		array( 'storefront-style' )
	);

}

/**
  * Remove WooCommerce default catalog ordering from shop page.
  */
  add_action( 'after_setup_theme', 'remove_woocommerce_catalog_ordering', 1 );

  function remove_woocommerce_catalog_ordering() {
	  remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 10 ); // If using Storefront, replace 30 by 10.
  }

  function remove_woocommerce_header_search() {
	  remove_action( 'storefront_header', 'storefront_product_search', 40);
  }
  add_action( 'after_setup_theme', 'remove_woocommerce_header_search' );

add_action( 'init', 'storefront_remove_storefront_breadcrumbs' );
function storefront_remove_storefront_breadcrumbs() {
   remove_action( 'storefront_before_content', 'woocommerce_breadcrumb', 10 );
}


function meks_which_template_is_loaded() {
	if ( is_super_admin() ) {
		global $template;
		print_r( $template );
	}
}
 
add_action( 'wp_footer', 'meks_which_template_is_loaded' );
